import { TestBed, inject } from "@angular/core/testing";

import { UserService } from "./user.service";
import {
  HttpTestingController,
  HttpClientTestingModule
} from "@angular/common/http/testing";
import { User, Role } from "../types/user";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";

describe("UserService", () => {
  let service, http, backend;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
  });

  beforeEach(inject(
    [UserService, HttpClient, HttpTestingController],
    (conf: UserService, h: HttpClient, b: HttpTestingController) => {
      service = conf;
      http = h;
      backend = b;
    }
  ));

  afterEach(inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      httpMock.verify();
    }
  ));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should return array of users when getAll method is called", () => {
    const mockMedias: User[] = [
      {
        id: "61a160ff-49d3-4b43-a4ab-b82d708f9c1d",
        email: "richard.dequeker@gmail.com",
        role: Role.User
      },
      {
        id: "691bbd2d-90c0-4eb3-bfdb-1d49980f5f47",
        email: "kevin.george@gmail.com",
        role: Role.User
      },
      {
        id: "14a28d83-fcdf-4285-9bc6-cff798c52362",
        email: "romain.ducros@gmail.com",
        role: Role.User
      }
    ];

    service.getAll().subscribe(users => {
      expect(users.length).toEqual(3);
    });

    const req = backend.expectOne(environment.serverUrl + "users");
    expect(req.request.method).toEqual("GET");
    req.flush(mockMedias);
  });
});
