import { TestBed, inject } from "@angular/core/testing";

import { MediaService } from "./media.service";
import {
  HttpTestingController,
  HttpClientTestingModule
} from "@angular/common/http/testing";
import { Media, MediaType } from "../types/media";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";

describe("MediaService", () => {
  let service, http, backend;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MediaService]
    });
  });

  beforeEach(inject(
    [MediaService, HttpClient, HttpTestingController],
    (conf: MediaService, h: HttpClient, b: HttpTestingController) => {
      service = conf;
      http = h;
      backend = b;
    }
  ));

  afterEach(inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      httpMock.verify();
    }
  ));

  it("should be created", () => {
    expect(service).toBeTruthy();
  });

  it("should return array of medias when getAll is called", () => {
    const mockMedias: Media[] = [
      {
        id: "61a160ff-49d3-4b43-a4ab-b82d708f9c1d",
        type: MediaType.Video,
        genre: "Action",
        title: "Snatch",
        author: "Guy Ritchie"
      },
      {
        id: "691bbd2d-90c0-4eb3-bfdb-1d49980f5f47",
        type: MediaType.Music,
        author: "Jam Baxter",
        genre: "Rap",
        title: "Caravan"
      },
      {
        id: "14a28d83-fcdf-4285-9bc6-cff798c52362",
        type: MediaType.Book,
        author: "Sun Tzu",
        genre: "Histoire",
        title: "L'art de la guerre"
      }
    ];

    service.getAll().subscribe(medias => {
      expect(medias.length).toEqual(3);
      expect(medias[0].type).toEqual(MediaType.Video);
    });

    const req = backend.expectOne(environment.serverUrl + "medias");
    expect(req.request.method).toEqual("GET");
    req.flush(mockMedias);
  });
});
