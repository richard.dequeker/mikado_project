import { TestBed, inject } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from './auth.service';
import { AuthPayload } from '../types/auth';
import { Role } from '../types/user';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

describe('AuthService', () => {
    let service, http, backend;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [AuthService]
        });
    });

    beforeEach(inject(
        [AuthService, HttpClient, HttpTestingController],
        (conf: AuthService, h: HttpClient, b: HttpTestingController) => {
            service = conf;
            http = h;
            backend = b;
        }
    ));

    afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
        httpMock.verify();
    }));

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return authenticated user with token when signIn method is called', () => {
        const mockAuthPayload: AuthPayload = {
            user: {
                id: '61a160ff-49d3-4b43-a4ab-b82d708f9c1d',
                role: Role.User,
                email: 'richard.dequeker@gmail.com'
            },
            token:
                'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
        };

        service.signIn('richard.dequeker@gmail.com', 'AAaa1234').subscribe(authPayload => {
            expect(authPayload).toEqual(mockAuthPayload);
        });

        const req = backend.expectOne(environment.serverUrl + 'auth/signin');
        expect(req.request.method).toEqual('POST');
        req.flush(mockAuthPayload);
    });
});
