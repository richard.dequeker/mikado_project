import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { User } from '../types/user';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private readonly baseUrl = environment.serverUrl + 'users';

    constructor(private http: HttpClient) {}

    getAll() {
        return this.http.get<User[]>(this.baseUrl);
    }

    get(id: string) {
        return this.http.get<User>(this.baseUrl + '/' + id);
    }

    update(id: string, data: any) {
        return this.http.put<User>(this.baseUrl + '/' + id, { body: data });
    }

    delete(id: string) {
        return this.http.delete<string>(this.baseUrl + '/' + id);
    }
}
