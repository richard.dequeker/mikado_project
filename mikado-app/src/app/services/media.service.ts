import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Media } from '../types/media';

@Injectable({
    providedIn: 'root'
})
export class MediaService {
    private readonly baseUrl = environment.serverUrl + 'medias';

    constructor(private http: HttpClient) {}

    getAll() {
        return this.http.get<Media[]>(this.baseUrl);
    }

    get(id: string) {
        return this.http.get<Media>(this.baseUrl + '/' + id);
    }

    update(id: string, data: any) {
        return this.http.put<Media>(this.baseUrl + '/' + id, { body: data });
    }

    delete(id: string) {
        return this.http.delete<string>(this.baseUrl + '/' + id);
    }
}
