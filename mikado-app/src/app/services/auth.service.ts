import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    baseUrl = environment.serverUrl + 'auth';

    constructor(private http: HttpClient) {}

    signIn(email: string, password: string) {
        return this.http.post(this.baseUrl + '/signin', { body: { email, password } });
    }

    signUp(email: string, password: string) {
        return this.http.post(this.baseUrl + '/signup', { body: { email, password } });
    }
}
