import { User } from './user';

export interface AuthPayload {
    user: User;
    token: string;
}
