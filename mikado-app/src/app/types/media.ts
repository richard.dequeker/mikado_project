export enum MediaType {
    Video = 0,
    Book = 1,
    Music = 2
}

export interface Media {
    id: string;
    type: MediaType;
    title: string;
    author: string;
    genre: string;
}
