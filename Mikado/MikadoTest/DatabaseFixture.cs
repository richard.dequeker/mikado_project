﻿using Microsoft.EntityFrameworkCore;
using Mikado.Data;
using Mikado.Models;
using Npgsql;
using System;
using System.Diagnostics;

namespace MikadoTest
{
    public class DatabaseFixture : IDisposable
    {

        public NpgsqlConnection Connection { get; private set; }
        public MikadoContext Context { get; private set; }

        public DatabaseFixture()
        {
            Connection = new NpgsqlConnection("Host=localhost;Database=mikado_test;Username=postgres;Password=root");

            var dbOptionsBuilder = new DbContextOptionsBuilder<MikadoContext>();
            var methodInfos = new StackTrace().GetFrame(1).GetMethod();
            var className = methodInfos.ReflectedType.ReflectedType != null ? methodInfos.ReflectedType.ReflectedType.Name : methodInfos.ReflectedType.Name;

            dbOptionsBuilder.UseNpgsql(Connection);

            Context = new MikadoContext(dbOptionsBuilder.Options);

            Context.Database.EnsureCreated();
            Connection.Open();

            InitialSeed();
        }

        public void Dispose()
        {
            Context.Database.EnsureDeleted();
        }

        public void InitialSeed()
        {
            // MEDIAS
            Context.Media.Add(new Media
            {
                Title = "L'art de la guerre",
                Author = "Sun Tzu",
                Genre = "Hitsoire",
                Type = MediaType.Book
            }); ;
            Context.Media.Add(new Media
            {
                Title = "Snatch",
                Author = "Guy Ritchie",
                Genre = "Action",
                Type = MediaType.Video
            });
            Context.Media.Add(new Media
            {
                Title = "Caravan",
                Author = "Jam Baxter",
                Genre = "Rap uk",
                Type = MediaType.Music
            });
            // USERS
            Context.User.Add(new User
            {
                Email = "richard.dequeker@gmail.com",
                Password = "AAaa1234*",
                Role = Role.User
            });
            Context.User.Add(new User
            {
                Email = "anthony.cordany@gmail.com",
                Password = "AAaa1234*",
                Role = Role.User
            });
            Context.SaveChanges();
        }
    }
}
