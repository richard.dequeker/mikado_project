﻿using FluentAssertions;
using Mikado.Data.Repositories;
using Mikado.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace MikadoTest.Repositories
{
    public class UserRepositoryTest: IClassFixture<DatabaseFixture>
    {
        private readonly DatabaseFixture _fixture;
        private readonly UserRepository _repository;

        public UserRepositoryTest(DatabaseFixture fixture)
        {
            _fixture = fixture;
            _repository = new UserRepository(_fixture.Context);
        }

        #region GetALL
        [Fact(DisplayName = "GetAll return user list")]
        public async void GetAll_return_user_list()
        {
            // When
            var users = await _repository.GetAll();
            // Expect
            users.Should().NotBeNullOrEmpty();
            users.Should().BeOfType(typeof(List<User>));
        }
        #endregion

        #region Get
        [Fact(DisplayName = "Get return user if exist")]
        public async void Get_return_user_if_exist()
        {
            // Where
            var user = await _repository.Get(new Guid("8c4ecfb9-606f-4c85-82a6-4bce1abb7729"));
            // Expect
            user.Should().NotBeNull();
            user.Should().BeOfType(typeof(User));
            user.Id.Should().Equals("8c4ecfb9-606f-4c85-82a6-4bce1abb7729");
        }

        [Fact(DisplayName = "Get return null if not exist")]
        public async void Get_return_null_if_not_exist()
        {
            // Where
            var user = await _repository.Get(new Guid());
            // Expect
            user.Should().BeNull();
        }

        [Fact(DisplayName = "Get throw FormatException if invalid uuid param")]
        public async void Get_throw_format_exception_if_invalid_uuid_param()
        {
            // When
            try
            {
                var user = await _repository.Get(new Guid(""));
            }
            // Expect
            catch (Exception e)
            {
                e.Should().BeOfType(typeof(FormatException));
            }
        }
        #endregion

        #region Add
        [Fact(DisplayName = "Add return persisted user")]
        public async void Add_return_persisted_user()
        {
            // Data
            var user = new User
            {
                Email = "test.user1@mail.com",
                Password = "AAaa1234*",
                Role = Role.User
            };
            // When
            var entity = await _repository.Add(user);
            entity.Should().NotBeNull();

            entity.Email.Should().Equals(user.Email);
            entity.Password.Should().Equals(user.Password);
            entity.Password.Should().Equals(user.Role);
        }
        #endregion

        #region Update
        [Fact(DisplayName = "Update return updated user")]
        public async void Update_return_updated_user()
        {
            // Data
            var user = await _repository.Add(new User
            {
                Email = "test.user2@mail.com",
                Password = "AAaa1234*",
                Role = Role.User
            });
            // When
            user.Password = "123456";
            var entity = await _repository.Update(user);
            // Expect
            entity.Should().NotBeNull();
            entity.Should().BeOfType(typeof(User));
            entity.Password.Should().Equals(user.Password);
        }
        #endregion

        #region Delete
        [Fact(DisplayName = "Delete return deleted user")]
        public async void Delete_return_deleted_user()
        {
            // When
            var user = await _repository.Delete(new Guid("41f15e6d-4ca5-46cb-979a-9f30549134a8"));
            // Expect
            user.Should().NotBeNull();
            user.Should().BeOfType(typeof(User));
        }
        #endregion
    }
}
