﻿

using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Mikado.Data.Repositories;
using Mikado.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace MikadoTest.Repositories
{
    public class MediaRepositoryTest: IClassFixture<DatabaseFixture>
    {

        private readonly DatabaseFixture _fixture;
        private readonly MediaRepository _repository;

        public MediaRepositoryTest(DatabaseFixture fixture)
        {
            _fixture = fixture;
            _repository = new MediaRepository(_fixture.Context);
        }

        #region GetALL
        [Fact(DisplayName = "GetAll return media list")]
        public async void GetAll_return_media_list()
        {
            // When
            var medias = await _repository.GetAll();
            // Expect
            medias.Should().NotBeNullOrEmpty();
            medias.Should().BeOfType(typeof(List<Media>));
        }
        #endregion

        #region Get
        [Fact(DisplayName = "Get return media if exist")]
        public async void Get_return_media_if_exist()
        {
            // Where
            // var media = await _repository.Get(new Guid("83cabc65-36ca-4fbc-93f7-d3bf6003af57"));
            // Expect
            // media.Should().NotBeNull();
            // media.Should().BeOfType(typeof(Media));
            // media.Id.Should().Equals("83cabc65-36ca-4fbc-93f7-d3bf6003af57");
        }

        [Fact(DisplayName = "Get return null if not exist")]
        public async void Get_return_null_if_not_exist()
        {
            // Where
            var media = await _repository.Get(new Guid());
            // Expect
            media.Should().BeNull();
        }
        
        [Fact(DisplayName = "Get throw FormatException if invalid uuid param")]
        public async void Get_throw_format_exception_if_invalid_uuid_param()
        {
            // When
            try
            {
                var media = await _repository.Get(new Guid(""));
            }
            // Expect
            catch (Exception e)
            {
                e.Should().BeOfType(typeof(FormatException));
            }
        }
        #endregion

        #region Add
        [Fact(DisplayName = "Add return persisted")]
        public async void Add_return_persisted_media()
        {
            // Data
            var media = new Media
            {
                Title = "Test media",
                Author = "Test author",
                Genre = "Test genre",
                Type = MediaType.Book
            };
            // When
            var entity = await _repository.Add(media);
            entity.Should().NotBeNull();

            entity.Title.Should().Equals(media.Title);
            entity.Author.Should().Equals(media.Author);
            entity.Genre.Should().Equals(media.Genre);
            entity.Type.Should().Equals(media.Type);
        }
        #endregion

        #region Update
        [Fact(DisplayName = "Update return updated media")]
        public async void Update_return_updated_media()
        {
            // Data
            var media = await _repository.Add(new Media
            {
                Id = new Guid("93ed9c91-153b-482f-bd03-07ad498792d1"),
                Title = "Test title",
                Genre = "Test genre",
                Author = "Test author",
                Type = MediaType.Book
            });
            // When
            media.Title = "Test title updated";
            var entity = await _repository.Update(media);
            // Expect
            entity.Should().NotBeNull();
            entity.Id.Should().Equals(media.Id);
            entity.Title.Should().Equals(media.Title);
            entity.Genre.Should().Equals(media.Genre);
            entity.Type.Should().Equals(media.Type);
        }
        #endregion

        #region Delete
        [Fact(DisplayName = "Delete return deleted media")]
        public async void Delete_return_deleted_media()
        {
            // When
            // var media = await _repository.Delete(new Guid("83cabc65-36ca-4fbc-93f7-d3bf6003af57"));
            // Expect
            // media.Should().NotBeNull();
            // media.Should().BeOfType(typeof(Media));
        }
        #endregion
    }
}
