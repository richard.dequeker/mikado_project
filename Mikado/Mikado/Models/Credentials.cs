﻿
using System.ComponentModel.DataAnnotations;

namespace Mikado.Models
{
    public class Credentials
    {
        /// <summary>
        /// User unique email
        /// </summary>
        [Required]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")]
        public string Email { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
    }
}
