﻿
namespace Mikado.Models
{
    public class MediaSearchParams
    {
        public MediaType? Type { get; set; }
    }
}
