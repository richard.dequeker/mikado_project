﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Mikado.Data;

namespace Mikado.Models
{
    /// <summary>
    /// Possible media types
    /// </summary>
    public enum MediaType
    {
        Video,
        Book,
        Music
    }
    public class Media : IEntity
    {
        /// <summary>
        /// Media id
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        /// <summary>
        /// Media type
        /// </summary>
        [Required]
        public MediaType Type { get; set; }
        /// <summary>
        /// Media title
        /// </summary>
        [Required]
        [StringLength(60)]
        public string Title { get; set; }
        /// <summary>
        /// Media author name
        /// </summary>
        [Required]
        [StringLength(60)]
        public string Author { get; set; }
        /// <summary>
        /// Media genre
        /// </summary>
        [Required]
        [StringLength(30)]
        public string Genre { get; set; }
    }
}
