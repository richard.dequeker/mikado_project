﻿
using Microsoft.IdentityModel.Tokens;

namespace Mikado.Models
{
    public class AuthPayload
    {
        /// <summary>
        /// Authenticated user
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Generated JWT token 
        /// </summary>
        public string Token { get; set; }

        public AuthPayload(User user, string token)
        {
            User = user;
            Token = token;
        }

    }
}
