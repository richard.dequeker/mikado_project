﻿using Mikado.Data;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mikado.Models { 

    /// <summary>
    /// Possible user roles
    /// </summary>
    public enum Role
    {
        User,
        Admin
    }

    public class User : IEntity
    {
        /// <summary>
        /// User id
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        /// <summary>
        /// User role
        /// </summary>
        [Required]
        public Role Role { get; set; }
        /// <summary>
        /// User unique email
        /// </summary>
        [Required]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$")]
        public string Email { get; set; }
        /// <summary>
        /// User password
        /// </summary>
        [Required]
        [MinLength(6)]
        public string Password { get; set; }
    }
}
