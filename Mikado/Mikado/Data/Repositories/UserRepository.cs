﻿using Mikado.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mikado.Data.Repositories
{
    public class UserRepository : RepositoryBase<User, MikadoContext>
    {
        public UserRepository(MikadoContext context): base(context)
        {

        }
        public async Task<User> GetByEmail(string email)
        {
            return _context.Set<User>().Where(u => u.Email == email).FirstOrDefault();
        }
    }
}
