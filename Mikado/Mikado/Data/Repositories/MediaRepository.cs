﻿using Mikado.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Mikado.Data.Repositories
{
    public class MediaRepository : RepositoryBase<Media, MikadoContext>
    {
        public MediaRepository(MikadoContext context) : base(context)
        {
        }

        public async Task<List<Media>> Search(MediaType? type)
        {
            if (type != null) { 
                return _context.Media.Where(m => m.Type == type).ToList();
            }
            return await GetAll();
        }
    }
}