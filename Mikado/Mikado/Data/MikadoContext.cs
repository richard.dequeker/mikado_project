﻿using Microsoft.EntityFrameworkCore;
using Mikado.Models;

namespace Mikado.Data
{
    public class MikadoContext : DbContext
    {
        public MikadoContext (DbContextOptions<MikadoContext> options)
            : base(options)
        {
        }
        public DbSet<User> User { get; set; }

        public DbSet<Media> Media { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .ToTable("users")
                .HasIndex(u => u.Email).IsUnique();

            modelBuilder.Entity<Media>().ToTable("medias");
        }
    }
}
