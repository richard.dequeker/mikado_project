﻿
using System;

namespace Mikado.Data
{
    /// <summary>
    /// Entity interface
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Generated uuid as primary key
        /// </summary>
        Guid Id { get; set; }
    }
}
