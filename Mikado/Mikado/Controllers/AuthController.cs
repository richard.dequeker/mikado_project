﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

using Mikado.Data.Repositories;
using Mikado.Models;
using Microsoft.AspNetCore.Http;

using Mikado.Helpers;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System;
using Microsoft.AspNetCore.Authorization;

namespace Mikado.Controllers
{
    [AllowAnonymous]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly UserRepository _userRepository;
        private readonly AppSettings _appSettings;

        public AuthController(UserRepository repository, IOptions<AppSettings> appSettings)
        {
            _userRepository = repository;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Sign into application
        /// </summary>
        /// <remarks>
        /// <param name="credentials">User credentials</param>
        /// <returns>Auth payload</returns>
        /// <response code="200">User info and JWT token</response>
        /// <response code="401">If invalid credentials</response>
        [HttpPost("signin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<AuthPayload>> SignIn(Credentials credentials)
        {
            var user = await _userRepository.GetByEmail(credentials.Email);
            // User exist ?
            if (user == null)
            {
                return new UnauthorizedResult();
            }
            // Password correct ?
            if (!BCrypt.Net.BCrypt.Verify(credentials.Password, user.Password))
            {
                return new UnauthorizedResult();
            }
            // Generate token
            string token = GenerateToken(user);

            // delete password
            user.Password = null;
            return new AuthPayload(user, token);
        }

        /// <summary>
        /// Register into application
        /// </summary>
        /// <remarks>
        /// <param name="credentials">User registration credentials</param>
        /// <response code="204">No content</response>
        /// <response code="400">If user already exist</response>
        [HttpPost("signup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> SignUp(Credentials credentials)
        {
            var user = await _userRepository.GetByEmail(credentials.Email);
            if (user != null)
            {
                return new BadRequestResult();
            }
            var hash = BCrypt.Net.BCrypt.HashPassword(credentials.Password);

            await _userRepository.Add(new User() { Email = credentials.Email, Password = hash, Role = Role.User });
            
            return new NoContentResult();
        }

        /// <summary>
        /// Generate a new Jwt token for user
        /// </summary>
        /// <param name="user">Authenticated user</param>
        /// <returns>A new Jwt token</returns>
        private string GenerateToken(User user) {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.JwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

    }
}
