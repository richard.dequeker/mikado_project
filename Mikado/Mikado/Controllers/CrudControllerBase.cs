﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Mikado.Data;
using Mikado.Data.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System;

namespace Mikado.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class CrudControllerBase<TEntity, TRepository> : ControllerBase
        where TEntity : class, IEntity
        where TRepository : IRepository<TEntity>
    {
        protected readonly TRepository repository;

        public CrudControllerBase(TRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <remarks>
        /// <param name="id">Entity id</param>
        /// <returns>A newly created Entity</returns>
        /// <response code="200">Returns all entities</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<TEntity>>> Get()
        {
            return await repository.GetAll();
        }

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <remarks>
        /// <param name="id">Entity id</param>
        /// <returns>A newly created Entity</returns>
        /// <response code="200">return the entity</response>
        /// <response code="404">If entity not exist</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TEntity>> Get(Guid id)
        {
            var entity = await repository.Get(id);
            if (entity == null)
            {
                return new NotFoundResult();
            }
            return entity;
        }

        /// <summary>
        /// Update an existing entity.
        /// </summary>
        /// <remarks>
        /// <param name="id">Entity id</param>
        /// <param name="entity">Entity data</param>
        /// <returns>A newly created Entity</returns>
        /// <response code="204">No content</response>
        /// <response code="400">If invalid params</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Put(Guid id, TEntity entity)
        {
            if (id != entity.Id)
            {
                return new BadRequestResult();
            }
            await repository.Update(entity);
            return new NoContentResult();
        }

        /// <summary>
        /// Create a new entity.
        /// </summary>
        /// <remarks>
        /// <param name="entity">Entity data</param>
        /// <returns>A newly created Entity</returns>
        /// <response code="201">Returns the newly created item</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<TEntity>> Post(TEntity entity)
        {
            await repository.Add(entity);
            return new CreatedAtActionResult(
                entity.GetType().ToString(),
                entity.GetType().ToString(),
                new { id = entity.Id },
                entity
            );
        }

        /// <summary>
        /// Delete an entity.
        /// </summary>
        /// <remarks>
        /// <param name="id">Entity id</param>
        /// <returns>A newly created Entity</returns>
        /// <response code="200">Returns the deleted entity</response>
        /// <response code="404">If entity not exist</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<TEntity>> Delete(Guid id)
        {
            var entity = await repository.Delete(id);
            if (entity == null)
            {
                return new NotFoundResult();
            }
            return entity;
        }
    }
}
