﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Mikado.Data.Repositories;
using Mikado.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Mikado.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MediasController : CrudControllerBase<Media, MediaRepository>
    {
        public MediasController(MediaRepository repository) : base(repository)
        {
        }

        [AllowAnonymous]
        [HttpGet("Search")]
        public async Task<ActionResult<List<Media>>> Search([FromQuery(Name = "type")] MediaType? type)
        {
            return await repository.Search(type);
        }
    }
}
