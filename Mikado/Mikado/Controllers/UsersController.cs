﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using Mikado.Data.Repositories;
using Mikado.Models;

namespace Mikado.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : CrudControllerBase<User, UserRepository>
    {
        public UsersController(UserRepository repository): base(repository)
        {
        }
    }
}
